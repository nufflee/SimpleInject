﻿using System;

namespace SimpleInject
{
    public interface IInjectableResolver
    {
        object Resolve<T>(IInjectableAttribute attribute, T instance, Type type);
    }

    public interface IInjectableResolver<in T> : IInjectableResolver where T : IInjectableAttribute
    {
        object Resolve<TS>(T attribute, TS instance, Type type);
    }
}