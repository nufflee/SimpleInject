﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SimpleInject
{
    public static class InjectableResolver
    {
        private static readonly Type[] ResolverTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(x => typeof(IInjectableResolver).IsAssignableFrom(x) && !x.IsInterface).ToArray();
        private static readonly Dictionary<Type, IInjectableResolver> ResolverLookup = new Dictionary<Type, IInjectableResolver>(ResolverTypes.Length);

        static InjectableResolver()
        {
            foreach (var type in ResolverTypes)
            {
                var genericType = type.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IInjectableResolver<>))?.GetGenericArguments().FirstOrDefault();

                if (genericType == null)
                {
                    throw new InjectException("Found IInjectableResolver without generic type. Use IInjectableResolver<T> when creating new resolver types.");
                }

                ResolverLookup[genericType] = (IInjectableResolver)Activator.CreateInstance(type);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IInjectableResolver Get<T>()
        {
            return Get(typeof(T));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IInjectableResolver Get(Type type)
        {
            if (ResolverLookup.TryGetValue(type, out IInjectableResolver resolver))
            {
                return resolver;
            }

            throw new InjectException($"Unable to locate resolver for type {type}");
        }
    }
}