﻿using System.Runtime.CompilerServices;

namespace SimpleInject
{
    public class Injector
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Inject<T>(T instance)
        {
            var info = ReflectCache.Get<T>();

            for (var i = 0; i < info.Length; ++i)
            {
                info[i].Inject(instance);
            }
        }
    }
}