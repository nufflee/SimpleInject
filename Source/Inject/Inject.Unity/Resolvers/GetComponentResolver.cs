﻿using System;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponent : Attribute, IInjectableAttribute
    {
    }

    internal class GetComponentResolver : Component, IInjectableResolver<GetComponent>
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(GetComponent attribute, T instance, Type type)
        {
            return (instance as Component).GetComponent(type);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((GetComponent)attribute, instance, type);
        }
    }
}