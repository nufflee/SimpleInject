﻿using NUnit.Framework;
using UnityEngine;

namespace SimpleInject.Tests
{
    public class Tests : MonoBehaviour
    {
        private void Start()
        {
            new TestGetComponent();
            new TestGetComponents();
            new TestGetComponentInChildren();
            new TestGetComponentsInChildren();
            new TestGetComponentInParent();
            new TestGetComponentsInParent();
            new TestMultiInject();
            new TestScriptableObjectCreateInstance();
            new TestInheritance();
            new TestCreateGameObject();

            Debug.Log("All tests pass!");
        }
    }

    internal class TestGetComponent
    {
        private class TestInject : MonoBehaviour
        {
            [GetComponent] private TargetComponent target;

            public TargetComponent Target => target;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestGetComponent()
        {
            var go = new GameObject();
            var target = go.AddComponent<TargetComponent>();
            var injected = go.AddComponent<TestInject>();
            Assert.NotNull(injected.Target);
            Assert.AreEqual(target, injected.Target);
        }
    }

    internal class TestGetComponents
    {
        private class TestInject : MonoBehaviour
        {
            [GetComponents] private TargetComponent[] targets;

            public TargetComponent[] Targets => targets;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestGetComponents()
        {
            var go = new GameObject();

            var target1 = go.AddComponent<TargetComponent>();
            var target2 = go.AddComponent<TargetComponent>();
            var target3 = go.AddComponent<TargetComponent>();
            var target4 = go.AddComponent<TargetComponent>();
            var target5 = go.AddComponent<TargetComponent>();

            var injected = go.AddComponent<TestInject>();

            var foundUnityTargets = go.GetComponents<TargetComponent>();

            Assert.NotNull(injected.Targets);
            Assert.AreEqual(5, injected.Targets.Length);
            Assert.AreEqual(target1, injected.Targets[0]);
            Assert.AreEqual(target2, injected.Targets[1]);
            Assert.AreEqual(target3, injected.Targets[2]);
            Assert.AreEqual(target4, injected.Targets[3]);
            Assert.AreEqual(target5, injected.Targets[4]);

            for (var i = 0; i < foundUnityTargets.Length; ++i)
            {
                Assert.AreEqual(foundUnityTargets[i], injected.Targets[i]);
            }
        }
    }

    internal class TestGetComponentInChildren
    {
        private class TestInject : MonoBehaviour
        {
            [GetComponentInChildren] private TargetComponent target;

            public TargetComponent Target => target;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestGetComponentInChildren()
        {
            var go = new GameObject();
            var childGo = new GameObject();
            childGo.transform.SetParent(go.transform);
            var target = childGo.AddComponent<TargetComponent>();

            Assert.IsNotNull(go.transform.GetChild(0).GetComponent<TargetComponent>());

            var injected = go.AddComponent<TestInject>();

            Assert.NotNull(injected.Target);
            Assert.AreEqual(target, injected.Target);
        }
    }

    internal class TestGetComponentsInChildren
    {
        private class TestInject : MonoBehaviour
        {
            [GetComponentsInChildren] private TargetComponent[] targets;

            public TargetComponent[] Targets => targets;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestGetComponentsInChildren()
        {
            var go = new GameObject();
            var childGo = new GameObject();
            childGo.transform.SetParent(go.transform);

            var target1 = childGo.AddComponent<TargetComponent>();
            var target2 = childGo.AddComponent<TargetComponent>();
            var target3 = childGo.AddComponent<TargetComponent>();
            var target4 = childGo.AddComponent<TargetComponent>();
            var target5 = childGo.AddComponent<TargetComponent>();

            var foundUnityTargets = go.GetComponentsInChildren<TargetComponent>();

            var injected = go.AddComponent<TestInject>();

            Assert.NotNull(injected.Targets);
            Assert.AreEqual(target1, injected.Targets[0]);
            Assert.AreEqual(target2, injected.Targets[1]);
            Assert.AreEqual(target3, injected.Targets[2]);
            Assert.AreEqual(target4, injected.Targets[3]);
            Assert.AreEqual(target5, injected.Targets[4]);

            for (var i = 0; i < foundUnityTargets.Length; ++i)
            {
                Assert.AreEqual(foundUnityTargets[i], injected.Targets[i]);
            }
        }
    }

    internal class TestGetComponentInParent
    {
        private class TestInject : MonoBehaviour
        {
            [GetComponentInParent] private TargetComponent target;

            public TargetComponent Target => target;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestGetComponentInParent()
        {
            var go = new GameObject();
            var target = go.AddComponent<TargetComponent>();

            var childGo = new GameObject();
            childGo.transform.SetParent(go.transform);

            Assert.IsNotNull(childGo.transform.parent.GetComponent<TargetComponent>());

            var injected = childGo.AddComponent<TestInject>();

            Assert.NotNull(injected.Target);
            Assert.AreEqual(target, injected.Target);
        }
    }

    internal class TestGetComponentsInParent
    {
        private class TestInject : MonoBehaviour
        {
            [GetComponentsInParent] private TargetComponent[] targets;

            public TargetComponent[] Targets => targets;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestGetComponentsInParent()
        {
            var go = new GameObject();
            var target1 = go.AddComponent<TargetComponent>();
            var target2 = go.AddComponent<TargetComponent>();
            var target3 = go.AddComponent<TargetComponent>();
            var target4 = go.AddComponent<TargetComponent>();
            var target5 = go.AddComponent<TargetComponent>();

            var childGo = new GameObject();
            childGo.transform.SetParent(go.transform);

            var foundUnityTargets = childGo.GetComponentsInParent<TargetComponent>();

            var injected = childGo.AddComponent<TestInject>();

            Assert.NotNull(injected.Targets);
            Assert.AreEqual(target1, injected.Targets[0]);
            Assert.AreEqual(target2, injected.Targets[1]);
            Assert.AreEqual(target3, injected.Targets[2]);
            Assert.AreEqual(target4, injected.Targets[3]);
            Assert.AreEqual(target5, injected.Targets[4]);

            for (var i = 0; i < foundUnityTargets.Length; ++i)
            {
                Assert.AreEqual(foundUnityTargets[i], injected.Targets[i]);
            }
        }
    }

    internal class TestScriptableObjectCreateInstance
    {
        private class TestInject : MonoBehaviour
        {
            [CreateInstance] private TestScriptableObject target;

            public TestScriptableObject Target => target;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestScriptableObjectCreateInstance()
        {
            var go = new GameObject();
            var injected = go.AddComponent<TestInject>();
            Assert.NotNull(injected.Target);
            Assert.AreEqual(typeof(TestScriptableObject), injected.Target.GetType());
        }
    }

    internal class TestMultiInject
    {
        private class TestInject : MonoBehaviour
        {
            [GetComponent] private TargetComponent target;
            [GetComponentInChildren] private TargetChildComponent childTarget;
            [GetComponentInParent] private TargetParentComponent parentTarget;

            public TargetComponent Target => target;
            public TargetChildComponent ChildTarget => childTarget;
            public TargetParentComponent ParentTarget => parentTarget;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestMultiInject()
        {
            var go = new GameObject();
            var target = go.AddComponent<TargetComponent>();

            Assert.IsNotNull(go.GetComponent<TargetComponent>());

            var childGo = new GameObject();
            childGo.transform.SetParent(go.transform);
            var childTarget = childGo.AddComponent<TargetChildComponent>();

            Assert.IsNotNull(go.transform.GetChild(0).GetComponent<TargetChildComponent>());

            var parentGo = new GameObject();
            go.transform.SetParent(parentGo.transform);
            var parentTarget = parentGo.AddComponent<TargetParentComponent>();

            Assert.IsNotNull(go.transform.parent.GetComponent<TargetParentComponent>());

            var injected = go.AddComponent<TestInject>();

            Assert.NotNull(injected.Target);
            Assert.NotNull(injected.ChildTarget);
            Assert.NotNull(injected.ParentTarget);

            Assert.AreEqual(target, injected.Target);
            Assert.AreEqual(childTarget, injected.ChildTarget);
            Assert.AreEqual(parentTarget, injected.ParentTarget);
        }
    }

    internal class TestInheritance
    {
        private abstract class TestAbstractInject : MonoBehaviour
        {
            [GetComponent] private TargetComponent target;

            public TargetComponent Target => target;
        }

        private class TestInject : TestAbstractInject
        {
            [GetComponent] private OtherTargetComponent otherTarget;

            public OtherTargetComponent OtherTarget => otherTarget;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestInheritance()
        {
            var go = new GameObject();
            var target = go.AddComponent<TargetComponent>();
            var otherTarget = go.AddComponent<OtherTargetComponent>();
            var injected = go.AddComponent<TestInject>();

            Assert.NotNull(injected.Target);
            Assert.NotNull(injected.OtherTarget);
            Assert.AreEqual(target, injected.Target);
            Assert.AreEqual(otherTarget, injected.OtherTarget);
        }
    }

    internal class TestCreateGameObject
    {
        private class TestInject : MonoBehaviour
        {
            [CreateGameObject] private GameObject defaults;
            [CreateGameObject(name: "Custom Name")] private GameObject customName;
            [CreateGameObject(new[] {typeof(TargetComponent)})] private GameObject target;
            [CreateGameObject("Everything", typeof(TargetComponent), typeof(OtherTargetComponent))] private GameObject everything;

            public GameObject Defaults => defaults;
            public GameObject CustomName => customName;
            public GameObject Target => target;
            public GameObject Everything => everything;

            private void Awake()
            {
                Injector.Inject(this);
            }
        }

        public TestCreateGameObject()
        {
            var go = new GameObject();
            var injected = go.AddComponent<TestInject>();

            Validate(injected.Defaults, "GameObject");

            Validate(injected.CustomName, "Custom Name");

            Validate(injected.Target, "GameObject");
            Assert.NotNull(injected.Target.GetComponent<TargetComponent>());

            Validate(injected.Everything, "Everything");
            Assert.NotNull(injected.Everything.GetComponent<TargetComponent>());
            Assert.NotNull(injected.Everything.GetComponent<OtherTargetComponent>());
        }

        private static void Validate(GameObject go, string name)
        {
            Assert.NotNull(go);
            Assert.AreEqual(go.name, name);
        }
    }
}