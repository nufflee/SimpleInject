Use attributes to define injection style:

[GetComponent] private TargetComponent target;

When ready to initialize component call:

Injector.Inject(this);

Example self-contained MonoBehaviour:

private class Example : MonoBehaviour
{
    [GetComponent] private AudioSource source;
    [GetComponentInChildren] private Collider collider;
    [GetComponentInChildren] private ParticleSystem particles;

    private void Awake()
    {
        // Can be called from anywhere
        Injector.Inject(this);
    }
}